BIN_DIR=_output/bin
CURRENT_DIR=$(shell pwd)

hello-world: init
	$(info Building executable...)
	CGO_ENABLED=0 GOARCH=amd64 go build -o ${BIN_DIR}/hello-world ./cmd/hello-world/

init:
	mkdir -p ${BIN_DIR}
